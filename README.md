To execute the program, run `sbt run` from the home directory. You 
will then be prompted to choose which problem to run: 0 for the Smallest 
Missing Integer problem, or 1 for the Peak Element problem. Enter your 
input array using space-separated numbers only.

Examples:
Enter 0 to access the Smallest Missing Integer problem and 1 to access 
the Peak Element problem:
0
Smallest Missing Integer: Enter the space-separated array to examine: 
-1 2 1 1 4 6 3 7 
The smallest missing integer is: 5

Enter 0 to access the Smallest Missing Integer problem and 1 to access 
the Peak Element problem: 
0
Smallest Missing Integer: Enter the space-separated array to examine: 
8 4 3 6 5 2 7 1 11
The smallest missing integer is: 9

Enter 0 to access the Smallest Missing Integer problem and 1 to access 
the Peak Element problem: 
1
Peak Element: Enter the space-separated array to examine: 
1 2 3 4 5 6 5 4 3 4 5 6 7 8 7
The index of a max element is: 5

Enter 0 to access the Smallest Missing Integer problem and 1 to access 
the Peak Element problem: 
1
Peak Element: Enter the space-separated array to examine: 
4 5 4 3 2 1
The index of a max element is: 1