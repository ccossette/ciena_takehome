package src.main.Main

object Main extends App {

  println("Enter 0 to access the Smallest Missing Integer problem and 1 to access the Peak Element problem: ")
  val selection = scala.io.StdIn.readLine().toInt

  if(selection != 0) {
    println("Peak Element: Enter the space-separated array to examine: ")
    val arr = scala.io.StdIn.readLine().split(" ").map(_.toInt)
    println("The index of a max element is: " + findPeak(arr, arr.length))
  } else {
    println("Smallest Missing Integer: Enter the space-separated array to examine: ")
    val arr2 = scala.io.StdIn.readLine().split(" ").map(_.toInt)
    println("The smallest missing integer is: " + smallestMissing(arr2))
  }

  def peakRecur(a: Array[Int], n: Int, low: Int, high: Int): Int = {
    //Pivot point
    var mid = (low + high)/2 + 1

    // Base case: stop recursing
    if (mid == 0) { return mid }

    // Compare the pivot with element to the right if not at end of list, otherwise compare left
    if(a(mid) >= a(mid-1) && a(mid) >= a(mid+1)) {
      return mid
    } else if (a(mid-1) > a(mid)){
      //Search left
      return peakRecur(a, n, low, mid-1)
    } else if (a(mid) < a(mid +1)){
      return peakRecur(a, n, low + 1, mid)
    }
    //Search right
    return peakRecur(a, n, mid+1, high)
  }

  def findPeak(a: Array[Int], n: Int): Int = {
    return peakRecur(a, n, 0, n-1)
  }

  def smallestMissing(arr2: Array[Int]): Int = {
    val arrSorted = arr2.sorted
    val len = arrSorted.length
    var index = 0
    var element = 1

    while (index < len) {
      //Dismiss Negatives and Zero
      if (arrSorted(index) > 0) {
        //Check for duplicates
        if (!(arrSorted(index) == element - 1)) {
          if (arrSorted(index) == element) {
            element += 1
          } else {
            //Found missing element
            return element
          }
        }
      }
      index += 1
    }
    //Otherwise output the next logical element
    return element
  }
}
